# OpenML dataset: Car-sales

https://www.openml.org/d/43619

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This is the Car sales data set which include information about different cars . This data set is being taken from the Analytixlabs for the purpose of prediction
In this we have to see two things
First we have see which feature has more impact on car sales and carry out result of this
Secondly we have to train the classifier and to predict car sales and check the accuracy of the prediction.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43619) of an [OpenML dataset](https://www.openml.org/d/43619). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43619/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43619/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43619/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

